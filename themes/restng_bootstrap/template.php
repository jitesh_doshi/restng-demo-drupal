<?php

/**
 * @file
 * template.php
 */

function restng_bootstrap_preprocess_node(&$vars, $hook) {
  if(isset($vars['node']) && isset($vars['view_mode'])) {
    $node_type = $vars['node']->type;
    $view_mode = $vars['view_mode'];
    $vars['theme_hook_suggestions'][] = "node__$view_mode";
    $vars['theme_hook_suggestions'][] = "node__${node_type}__$view_mode";
  }
}

/**
 * Overrides the 'system_powered_by' theme function, and adds a SpinSpire link.
 */
function restng_bootstrap_system_powered_by() {
  return '<span>' . t('Powered by <a href="@poweredby">Drupal</a> and <a href="@spinspire">SpinSpire</a>', array('@poweredby' => 'http://drupal.org', '@spinspire' => 'https://spinspire.com')) . '</span>';
}
