<?php
/**
 * @file
 * restng_config.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function restng_config_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access resource node'.
  $permissions['access resource node'] = array(
    'name' => 'access resource node',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'restws',
  );

  return $permissions;
}
