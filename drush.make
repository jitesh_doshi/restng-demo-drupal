; This file will download the projects used by this site
; Run it as ...
; drush make --no-core --contrib-destination=. drush.make .

core = 7.x
api = 2

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

projects[entity][version] = "1.5"
projects[entity][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[features][version] = "2.2"
projects[features][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[ctools][version] = "1.4"
projects[ctools][subdir] = "contrib"

projects[wysiwyg][version] = "2.2"
projects[wysiwyg][subdir] = "contrib"

libraries[tinymce][download][type] = get
libraries[tinymce][download][url] = http://download.moxiecode.com/tinymce/tinymce_3.5.11.zip
libraries[tinymce][directory_name] = tinymce
libraries[tinymce][destination] = libraries

projects[bootstrap][version] = "3.0"

projects[jquery_update][version] = "2.4"
projects[jquery_update][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[media][version] = "1.4"
projects[media][subdir] = "contrib"

projects[colorbox][version] = "2.8"
projects[colorbox][subdir] = "contrib"

libraries[colorbox][download][type] = get
libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox/archive/1.x.zip
libraries[colorbox][directory_name] = colorbox
libraries[colorbox][destination] = libraries

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[ds][version] = "2.6"
projects[ds][subdir] = "contrib"

projects[restws][version] = "2.2"
projects[restws][subdir] = "contrib"

libraries[restng][download][type] = "git"
libraries[restng][download][url] = "http://git.drupal.org/sandbox/jitesh_doshi/2125941.git"
libraries[restng][directory_name] = "restng"
libraries[restng][destination] = modules/custom

libraries[jwt.php][download][type] = get
libraries[jwt.php][download][url] = https://raw.githubusercontent.com/firebase/php-jwt/master/Authentication/JWT.php
libraries[jwt.php][directory_name] = jwt
libraries[jwt.php][destination] = libraries

libraries[stripe][download][type] = "git"
libraries[stripe][download][url] = "https://github.com/stripe/stripe-php.git"
libraries[stripe][directory_name] = stripe
libraries[stripe][destination] = modules/custom/restng/apps/restng-demo
